import VueRouter from 'vue-router';

let routes = [
    {
        component: require('./views/Home').default,
        path: '/',
    },
    {
        component: require('./views/About').default,
        path: '/about',
    },
    {
        component: require('./views/Contact').default,
        path: '/contact',
    },
];

export default new VueRouter({
    routes,
    linkActiveClass: 'is-active',
});
